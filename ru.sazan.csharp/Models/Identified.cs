﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ru.sazan.csharp.Models
{
    public interface Identified
    {
        int Id { get; }
    }
}

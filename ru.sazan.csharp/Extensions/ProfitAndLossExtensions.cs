﻿using ru.sazan.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ru.sazan.csharp.Extensions
{
    public static class ProfitAndLossExtensions
    {
        public static double GetProfitAndLossPoints(this IEnumerable<Trade> tradeList)
        {
            return tradeList.Sum(t => t.Sum) * -1;
        }
    }
}
